package com.example.demo.repository;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.model.Investigadores;


public interface InvestigadoresRepository extends JpaRepository<Investigadores,Integer>{

	@Query(value="SELECT DISTINCT  i.* FROM Equipos e,Reserva r,Investigadores i WHERE i.DNI=r.DNI AND r.numero_serie=r.numero_serie AND e.facultad BETWEEN ?1 AND ?2 ORDER BY i.DNI DESC",nativeQuery = true)
	List<Investigadores> buscarInvestigadores_queReservoEquipo_facultad(Integer a1,Integer a2);
	
	@Query("select i from Investigadores i where i.DNI>?1")
	List<Investigadores> investigadoresIdMayor(Integer num);
}
