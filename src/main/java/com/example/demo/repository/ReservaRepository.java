package com.example.demo.repository;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.model.Reserva;



public interface ReservaRepository extends JpaRepository<Reserva,Integer> {


	@Query(value="select * from Reserva",nativeQuery = true)
	List<Reserva> buscarReserva();
}
