package com.example.demo.repository;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
//import org.springframework.data.jpa.repository.Query;
import com.example.demo.model.Equipos;


@Repository("EquiposRepository")
public interface EquiposRepository extends JpaRepository<Equipos,Integer> {


	List<Equipos> findByNumeroSerieBetween(Integer id1,Integer id2);
	List<Equipos> findByNombreOrNumeroSerie(String nombre,Integer id);
	List<Equipos> findFirst8ByNumeroSerieLessThan(Integer id);//con limite a 5
	List<Equipos> findByNumeroSerieLessThanEqual(Integer id);
	List<Equipos> findByNombreIsNull();
	List<Equipos> findByNombreIsNotNull();
	List<Equipos> findByNombreLike(String n);
	List<Equipos> findByNombreNotLike(String n);
	List<Equipos> findByNombreContaining(String n);
	List<Equipos> findByNumeroSerieIn(Collection<Integer> id);
	Optional<Equipos> findByNombreIgnoreCase(String nombre);
	
//consulta de multitabla
	  @Query(value = "select e.* from equipos e,reserva r where r.numero_serie=e.numero_serie and r.comienzo BETWEEN '2019-11-01' and'2020-02-28'",nativeQuery=true)
	//@Query(value = "select * from equipos where facultad=1",nativeQuery = true)
	List<Equipos> buscarEquipoPorFechas();
	  
	@Query(value = "select e.* from equipos e,reserva r,investigadores i where i.DNI=r.DNI and r.numero_serie=e.numero_serie and i.nombre=?1",nativeQuery = true)
	List<Equipos> buscarEquiposXNombre_de_investigadores(String nombre);
	
	

}
