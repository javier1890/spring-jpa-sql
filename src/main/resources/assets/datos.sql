--Infomarcion del sistema ordenada y organizada para acceder a ella facilmente.

--FACULTADES
insert into facultad (codigo, nombre) values (0, 'ingenieria');
insert into facultad (codigo, nombre) values (1, 'educacion');
insert into facultad (codigo, nombre) values (2, 'ciencias fisicas');
insert into facultad (codigo, nombre) values (3, 'derecho');
insert into facultad (codigo, nombre) values (4, 'medicina');
insert into facultad (codigo, nombre) values (5, 'ciencias quimicas');
insert into facultad (codigo, nombre) values (20, 'agronomia');

--INVESTIGADORES
insert into investigadores (DNI, nombre,apellido, facultad) values (1, 'Rogelio','Maturana', 0);
insert into investigadores (DNI, nombre,apellido, facultad) values (2, 'Davin','Marin', 1);
insert into investigadores (DNI, nombre,apellido, facultad) values (3, 'Fanita','Perez', 2);
insert into investigadores (DNI, nombre,apellido, facultad) values (4, 'Juanito','Rojas', 3);
insert into investigadores (DNI, nombre,apellido, facultad) values (5, 'John','Freeman', 2);
insert into investigadores (DNI, nombre,apellido, facultad) values (6, 'Marcos','Freeman', 2);
insert into investigadores (DNI, nombre,apellido, facultad) values (7, 'Fernando','Freeman', 20);
insert into investigadores (DNI, nombre,apellido, facultad) values (8, 'Sofia','Freeman', 20);
insert into investigadores (DNI, nombre,apellido, facultad) values (9, 'Belen','Freeman', 2);
insert into investigadores (DNI, nombre,apellido, facultad) values (10, 'Camila','Freeman', 4);

--EQUIPOS
insert into equipos (numero_serie, nombre, facultad) values (1, 'Equipo de analisis sanguineo', 4);
insert into equipos (numero_serie, nombre, facultad) values (2, 'respirador mecanico', 4);
insert into equipos (numero_serie, nombre, facultad) values (3, 'Equipo de espectroscopia', 2);
insert into equipos (numero_serie, nombre, facultad) values (4, 'Supercomputador', 5);
insert into equipos (numero_serie, nombre, facultad) values (5, 'Supercomputador HP', 5);
insert into equipos (numero_serie, nombre, facultad) values (6, 'Equipo numero 9', 1);
insert into equipos (numero_serie, nombre, facultad) values (7, 'Maquina 45-RR0094 plus', 5);

--RESERVAS
insert into reserva (id_reserva, DNI, numero_serie, comienzo, fin) values (1,10,3,'2016-01-22', '2016-12-22');
insert into reserva (id_reserva, DNI, numero_serie, comienzo, fin) values (2,9,3, '2017-01-22', '2017-12-22');
insert into reserva (id_reserva, DNI, numero_serie, comienzo, fin) values (3,5,1, '2018-01-28', '2018-12-22');
insert into reserva (id_reserva, DNI, numero_serie, comienzo, fin) values (4,3,5, '2019-01-27', '2019-12-22');
insert into reserva (id_reserva, DNI, numero_serie, comienzo, fin) values (5,2,7, '2020-01-13', '2020-12-22');
insert into reserva (id_reserva, DNI, numero_serie, comienzo, fin) values (6,1,5, '2016-05-12', '2016-12-22');
insert into reserva (id_reserva, DNI, numero_serie, comienzo, fin) values (7,7,5, '2017-06-21', '2017-11-22');
insert into reserva (id_reserva, DNI, numero_serie, comienzo, fin) values (8,9,1, '2018-07-20', '2018-10-22');
insert into reserva (id_reserva, DNI, numero_serie, comienzo, fin) values (9,10,1,'2019-08-07', '2019-12-22');
insert into reserva (id_reserva, DNI, numero_serie, comienzo, fin) values (10,1,2,'2020-09-20', '2020-10-22');